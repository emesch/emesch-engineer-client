define(['logger'], function(logger) {
  
  function webinarHomeController($state, $stateParams, $uibModal, webinarService) {
    
    var self = this;
    
    var modalWebinarCreator;
    
    self.onOpenWebinarCreator = function() {
      modalWebinarCreator = $uibModal.open({        
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: 'partials/webinar/modals/webinar_creator.html',
        controller: 'webinarCreatorController',
        controllerAs: 'webinarCtrl'      
      });  
    }
    
    init();   
    
    function init() {
      
    }    
        
  }
  
  webinarHomeController.$inject = ['$state', '$stateParams', '$uibModal', 'webinarService'];
  
  return webinarHomeController;
  
});