define(['logger'], function(logger) {
  
  function webinarCreatorController($state, $stateParams, $uibModalInstance, webinarService) {
    
    var self = this;
      
    self.alerts = [];
      
    self.newWebinar = {
        title: undefined,
        description: undefined,
        autostart: undefined
    }
    
    self.onCreateWebinar = function() {
      console.log('Creating webinar...');
      sendNewWebinarRequestAndGoToWebinar();      
    }
    
    self.onCancelCreation = function() {
      console.log('Canceling creation...');
      $uibModalInstance.dismiss('cancel');    
    }
    
    init();   
    
    function init() {
      console.log('webinar controller initialization');  
    }
      
    function sendNewWebinarRequestAndGoToWebinar() {
      var newWebinarData = {
          title: self.newWebinar.title,
          description: self.newWebinar.description,
          autostart: self.newWebinar.autostart
      };
        
      webinarService.createNewWebinar(newWebinarData, doWhenWebinarCreationSuccess, doWhenWebinarCreationFailed);    
    }
      
    function doWhenWebinarCreationSuccess(response) {
      logger.info("Webinar creation success! ");
      logger.info("Logging response: ")
      logger.info(response);
      var newWebinarId = response.data.elements.webinarObject.value.id;
      webinarService.goToWebinarPageByResponseData(newWebinarId, response.data);
      $uibModalInstance.close();
    }
      
    function doWhenWebinarCreationFailed(response) {
      logger.debug("Webinar creation failed! ");  
    }
        
  }
  
  webinarCreatorController.$inject = ['$state', '$stateParams', '$uibModalInstance', 'webinarService'];
  
  return webinarCreatorController;
  
});