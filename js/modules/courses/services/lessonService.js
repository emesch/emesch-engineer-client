'use strict';

define(['logger'], function(logger) {

  function lessonService(envService, authenticationService, $state) {
    
    var domainUrl = envService.read('domainUrl');
    var createLessonApiUrl = envService.read('createLessonApiUrl');
    var getCoursesApiUrl = envService.read('getCoursesApiUrl');
        
    return {
      createNewLesson: createNewLesson,
      goToLessonPage: goToLessonPage
    };    
    
    function createNewLesson(newLessonData, doWhenLessonCreationSuccess, doWhenLessonCreationFailed) {           
      logger.info("Sending request with newLessonData: " + newLessonData.chapterId + ", " + newLessonData.title + ", " + newLessonData.description);
      authenticationService.performRequest('POST', createLessonApiUrl, newLessonData, doWhenLessonCreationSuccess, doWhenLessonCreationFailed);
    }
    
    function goToLessonPage(courseObject, lessonObject) {
      console.log("Going to lesson...");            
      getLessonJsonById(courseObject.id, lessonObject.id, function(response){
        goToLessonPageByResponseData(courseObject.id, lessonObject.id, response.data)  
      }, function(response) {
        $state.go('browseCourses');   
      });      
    }
    
    function goToLessonPageByResponseData(courseId, lessonId, responseData) {
      var responseView = responseData.view;
      var responseElements = responseData.elements;
      var responseErrors = responseData.errors;      
      
      $state.go('lessonPage', {courseId: courseId, lessonId: lessonId, view: responseView, elements: responseElements, errors: responseErrors});
    }
    
    function getLessonJsonById(courseId, lessonId, doWhenGettingLessonSuccess, doWhenGettingLessonFailed) {
      var jsonUrl = domainUrl+getCoursesApiUrl+"/"+courseId+"/"+lessonId;      
      authenticationService.performRequest('GET', jsonUrl, null, doWhenGettingLessonSuccess, doWhenGettingLessonFailed);    
    }
    
  }
  
  lessonService.$inject = ['envService', 'authenticationService', '$state'];
  
  return lessonService;
  
});