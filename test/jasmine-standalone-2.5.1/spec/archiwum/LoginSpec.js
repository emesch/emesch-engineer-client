describe("Edu", function() {          
  
});

describe("User Login/Registration", function() {
      
  beforeEach(module('app'));
  
  var $controller;
  var controller, $scope;
  var testData = {
    firstName: "Tasty",
    lastName: "Test",
    email: "test@emesch.pl",
    password: "somePassword"    
  }
  
  beforeEach(inject(function(_$controller_){
    $controller = _$controller_;          
  }));
  
  beforeEach(function() {
    $scope = {};
    controller = $controller('loginController', { $scope: $scope });     
  });
  
  it("should be no user logged in", function() {
    
  });
  
  describe("Register test", function() {
    it("validation info should be empty", function() {       
      expect(controller.validationInfo).toBe("");        
    });
        
    it("should not register existing test user", inject(function($http) {
      controller.registerInfo.email = testData.email;
      controller.registerInfo.firstName = testData.firstName;
      controller.registerInfo.lastName = testData.lastName;
      controller.registerInfo.password = testData.password;
      
      controller.registerUser();
      
      
      
    }));
  });
  
  describe("Login test", function() {
    it("should fail with non-existing user", inject(function($http) {            
         
    }));
    
    it("should fail with wrong password", inject(function($http) {            
      
    }));
    
    it("should logg in to test user", inject(function($http) {            
      
    }));
    
  });
  
  
    
    
});     