define(['logger'], function(logger) {
  
  function lessonElCreatorController($state, $stateParams, $uibModalInstance, FileUploader, lessonService) {
    
    var self = this;
    
    self.courseObject = null;
    self.lessonObject = null;
    
    self.alerts = [];
    self.imgUploader = new FileUploader();
    self.audioUploader = new FileUploader();
    
    self.newElement = {
      lessonId: undefined,
      title: undefined,
      contentType: 'text',
      content: {}
    }
    
    self.onElementTypeChange = function() {
      console.log('onElementTypeChange: ' + self.newElement.contentType);
    };
    
    self.onImgSelection = function(element) {
      console.log("onImgSelection" + element);
    }
    
    self.onCreateElement = function() {
      sendRequestWithNewElementData();      
    }
    
    self.onCreateElementAndClose = function() {
      sendRequestWithNewElementData();      
    }
    
    self.onCancelCreation = function() {      
      $uibModalInstance.dismiss('cancel');    
    }
    
    init();   
    
    function init() {
      console.log('lessonCreatorController initialization'); 
      self.courseObject = $stateParams.elements.courseObject;
      self.lessonObject = $stateParams.elements.lessonObject; 
    }
    
    function sendRequestWithNewElementData() {
      self.newElement.lessonId =  self.lessonObject.id;
      console.log("Image: ");
      console.log(self.newElement.content.image);
      
      //lessonService.createNewLessonElement(self.newElement, doWhenElementCreationSuccess, doWhenElementCreationFailed);      
    }
    
    function doWhenElementCreationSuccess(response) {
      logger.debug("Element creation succes! ");      
      $uibModalInstance.close();
    }

    function doWhenElementCreationFailed(response) {     
      logger.error("Element creation failed! ");
      if(response.data.errors[0]) {
        addAlert(response.data.errors[0].message);     
      }          
    }
    
    function addAlert(alertMsg) {
      self.alerts[0] = ({msg: alertMsg});
    }
        
  }
  
  lessonElCreatorController.$inject = ['$state', '$stateParams', '$uibModalInstance', 'FileUploader', 'lessonService'];
  
  return lessonElCreatorController;
  
});