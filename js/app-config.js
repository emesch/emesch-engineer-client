'use strict';

define(
  ['logger'],
  function(logger) {
    
    function appConfig($urlProvider, $stateProvider, envServiceProvider) {      
      
      $urlProvider.otherwise('/');

      $stateProvider
        .state('home', {
          url: '/',
          templateUrl: 'partials/home.html'
        })
        .state('loginNeeded', {
          url: '/',
          templateUrl: 'partials/users/login_needed.html'
        })
        .state('registerUser', {
          url: '/register',
          templateUrl: 'partials/users/register_user.html' 
        })
        .state('createNewCourse', {
          url: '/new_course',
          templateUrl: 'partials/courses/new_course.html'
        })
		.state('browseMyCourses', {
		  url: '/my_courses',
		  templateUrl: 'partials/courses/my_courses.html'
		})
        .state('browseCourses', {
          url: '/courses',
          templateUrl: 'partials/courses/browse_courses.html'
        })
       .state('coursePage', {
          url: '/courses/{courseId}',
          params: {
            view: null,
            elements: null,
            errors: null
          },
          templateUrl: 'partials/courses/course_page.html'
        })
      .state('lessonPage', {
        url: '/courses/{courseId}/{lessonId}',
        params: {
          view: null,
          elements: null,
          errors: null
        },
        templateUrl: 'partials/lessons/lesson_page.html' 
      
      })
      .state('webinarHome', {
        url: '/webinar/home',
        templateUrl: 'partials/webinar/webinar_home.html'
      })
      .state('webinarPage', {
        url: '/webinar/{webinarId}',
        params: {
          view: null,
          elements: null,
          errors: null
        },
        templateUrl: 'partials/webinar/webinar_page.html'
      });

      envServiceProvider.config({
        domains: {
          development: ['localhost']
        },
        vars: {
          development: {
            domainUrl: '//localhost:8182',
            
            registerUser: '//localhost:8182/api/users/register',
            loginUser: 'http://localhost:8182/auth',
            
            createCourseApiUrl: '/api/courses/new',
            createLessonApiUrl: '/api/lessons/new',
            
            getCoursesApiUrl: '/api/courses',
            getPopularCoursesApiUrl: '/api/courses/popular',
            getMyCoursesApiUrl: '/api/courses/my',
              
            getWebinarsOnlineApiUrl: '/api/webinar/online',
            getWebinarsOfflineApiUrl: '/api/webinar/offline',
            createWebinarApiUrl: '/api/webinar/new',
            joinWebinarApiUrl: '/api/webinar/'
          }		
        }	
      });

      envServiceProvider.check();     
    
    }
    
    appConfig.$inject = ['$urlRouterProvider', '$stateProvider', 'envServiceProvider'];
    
    return appConfig;
    
  }
);
  

