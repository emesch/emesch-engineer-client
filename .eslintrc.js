module.exports = {
    "env": {
        "browser": true,
        "es6": true
    },
    "extends": "eslint:recommended",
    "parserOptions": {
        "sourceType": "module"
    },
    "rules": {
        "indent": [
            "error",
            2
        ],       
        "semi": [
            "error",
            "always"
        ]
    },
    "globals": {
        "angular": 1,
        "module": 1,
        "inject": 1,
        
        "describe": 1,
        "it": 1,
        "expect": 1,
        "beforeEach": 1
    }
};