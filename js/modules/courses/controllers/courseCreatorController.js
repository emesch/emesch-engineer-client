define(['logger'], function(logger) {

  function courseCreatorController($window, envService, authenticationService, $state, coursesService) {
    
    var self = this;
    
    self.newCourse = {
      title: undefined,
      description: undefined,
      scope: undefined
    };
    
    self.saveNewCourse = function() {
      if(self.isLoggedIn()) {
        logger.info("Is logged in, so sending request.");
        sendRequestWithNewCourseData();
      } else {
        logger.info("You have to log in!");
        $state.go('loginNeeded');
      }  
    };
    
    self.cancelNewCourseCreation = function() {
      $state.go('home');
    };
    
    self.isLoggedIn = function() {
      return authenticationService.isLoggedIn();      
    };
    
    function sendRequestWithNewCourseData() {      
      var newCourseData = {
        title: self.newCourse.title,
        description: self.newCourse.description,
        scope: self.newCourse.scope
      };      
      
      coursesService.createNewCourse(newCourseData, doWhenCourseCreationSuccess, doWhenCourseCreationFailed);   
        
    }
    
    function doWhenCourseCreationSuccess(response) {
      logger.info("Course creation succes! ");      
      
      var getCourseAsJsonApiUrl = getNewCourseApiUrl(response);
      
      logger.info("New course url to get json data: " + getCourseAsJsonApiUrl);      
      coursesService.getCourseJson(getCourseAsJsonApiUrl, doWhenGettingNewCourseSuccess, doWhenGettingNewCourseFailed);           
    }

    function doWhenCourseCreationFailed(error) {
      logger.error("Course creation failed! "); 
    }
    
    function doWhenGettingNewCourseSuccess(response) {      
      logger.info("Getting new course succes! ");
      var newCourseId = response.data.elements.courseObject.value.id;      
      
      coursesService.goToCoursePageByResponseData(newCourseId, response.data);            
    }
    
    function doWhenGettingNewCourseFailed(error) {
      logger.error("Getting new course failed");  
    }
    
    function getNewCourseApiUrl(response) {
      return response.data.elements.newCourseUrl.value;
    }
    
  }
  
  courseCreatorController.$inject = ['$window', 'envService', 'authenticationService', '$state', 'coursesService'];
  
  return courseCreatorController;
  
});