'use strict';

define(['logger'], function(logger) {
   
  function navbarController($window, $state, $uibModal, authenticationService, coursesService, lessonService){
    
    var self = this;
    
    var modalElementCreator;
    
    self.navState = $state;
    
    self.currentStateName = function() {
      return self.navState.current.name;  
    }
    
    self.isVisibleHomeNavPanel = function() {      
      return self.navState.current.name != 'lessonPage';
    }
    
    self.isVisibleLessonNavPanel = function() {      
      return self.navState.current.name == 'lessonPage';
    }
    
    self.onNewCourse = function() {
      $state.go('createNewCourse');  
    };
      
    self.onBrowseMyCourses = function() {
      $state.go('browseMyCourses');        
    }
    
    self.onWebinarium = function() {
      $state.go('webinarHome');
    }
    
    self.onReturnToCourse = function() {      
      coursesService.goToCoursePage($state.params.courseId);
    }
    
    self.onNewElement = function() {
      modalElementCreator = $uibModal.open({        
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: 'partials/lessons/modals/lesson_element_creator.html',
        size: 'lg',
        controller: 'lessonElCreatorController',
        controllerAs: 'elementCtrl',        
      });  
    }
    
    self.logout = function() {
      logger.debug("Logout: deleting jwtToken from localStorage...")
      delete $window.localStorage['jwtToken'];
    };
    
    self.isLoggedIn = function() {        
      return authenticationService.isLoggedIn();
    };        
    
  }
    
  navbarController.$inject = ['$window', '$state', '$uibModal', 'authenticationService', 'coursesService', 'lessonService'];
    
  return navbarController;
    
});

