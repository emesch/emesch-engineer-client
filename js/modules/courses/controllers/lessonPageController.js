define(['logger'], function(logger) {
  
  function lessonPageController($state, $stateParams, $uibModal, coursesService, lessonService, pageHelperService) {
    
    var self = this;    
    
    self.courseObject = null;
    self.lessonObject = null;
    
    self.getLessonElements = function() {
      
    };
    
    init();   
    
    function init() {      
      logger.info("LessonPageController initialization...");
      
      if(!$stateParams.courseId || !$stateParams.lessonId) {
        $state.go('browseCourses');  
      }
      
      if(isInconsistentStateParams()) {
        logger.info("Need to reload.");
        reloadLessonPage();  
      }else{
        self.courseObject = $stateParams.elements.courseObject.value;
        self.lessonObject = $stateParams.elements.lessonObject.value;
      }      
    }
    
    function isInconsistentStateParams() {
      if(isNoElementsMapInStateParams()) {
        logger.info("No elements map in state params. ");
        return true;
      }
      if(isDifferentCourseIdInStateParams() || isDifferentLessonIdInStateParams()) {
        logger.info("Different ids in state params. ");
        return true;
      }
      
      return false;
    }
    
    function isNoElementsMapInStateParams() {      
      return !$stateParams.elements;
    }
    
    function isDifferentCourseIdInStateParams() {
      return $stateParams.courseId != $stateParams.elements.courseObject.value.id; 
    }
    
    function isDifferentLessonIdInStateParams() {
      return $stateParams.lessonId != $stateParams.elements.lessonObject.value.id; 
    }
    
    function reloadLessonPage() {
      logger.info("Reloading course page...");      
      coursesService.goToCoursePage($stateParams.courseId);    
    }
    
  }
  
  lessonPageController.$inject = ['$state', '$stateParams', '$uibModal',  'coursesService', 'lessonService', 'pageHelperService'];
  
  return lessonPageController;
  
});