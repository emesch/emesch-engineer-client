define(['logger'], function(logger) {
  
  function coursePageController($state, $stateParams, $uibModal, coursesService, lessonService) {
    
    var self = this;
    
    var modalLessonCreator;
    
    self.courseObject = null;
    
    self.getCreatorName = function() {
      if(self.courseObject) {
        return self.courseObject.instructors[0].elements[0].value;      
      }      
    };
    
    self.getChapters = function() {      
      if(self.courseObject) {
        return self.courseObject.chapters;
      }
    };
    
    self.getLessons = function(chapter) {
      return chapter.lessons;
    }    
    
    self.onOpenLessonCreator = function() {      
      modalLessonCreator = $uibModal.open({        
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: 'partials/courses/modals/lesson_creator.html',
        controller: 'lessonCreatorController',
        controllerAs: 'lessonCtrl',        
      });  
    }
    
    self.onLessonClick = function(lessonObject) {
      console.log(lessonObject);      
      lessonService.goToLessonPage(self.courseObject, lessonObject);
    }
    
    init();   
    
    function init() {      
      logger.debug("CoursePageController initialization...");
      logger.debug($stateParams);
    
      if(!$stateParams.courseId) {
        $state.go('browseCourses');  
      }
    
      if(isInconsistentStateParams()) {
        logger.info("Need to reload.");
        reloadCoursePage();  
      }else{
        self.courseObject = $stateParams.elements.courseObject.value;
      }
    }
    
    function isInconsistentStateParams() {
      if(isNoElementsMapInStateParams()) {
        logger.info("No elements map in state params. ");
        return true;
      }
      if(isDifferentCourseIdInStateParams()) {
        logger.info("Different ids in state params. ");
        return true;
      }
      
      return false;
    }
    
    function isNoElementsMapInStateParams() {      
      return !$stateParams.elements;
    }
    
    function isDifferentCourseIdInStateParams() {
      return $stateParams.courseId != $stateParams.elements.courseObject.value.id; 
    }
    
    function reloadCoursePage() {
      logger.info("Reloading course page...");      
      coursesService.goToCoursePage($stateParams.courseId);    
    }    
    
  }
  
  coursePageController.$inject = ['$state', '$stateParams', '$uibModal',  'coursesService', 'lessonService'];
  
  return coursePageController;
  
});