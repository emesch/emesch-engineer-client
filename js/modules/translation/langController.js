'use strict';

define(['logger'], function(logger){  
  
  function langController($translate){
    logger.debug("Defining langController...");
    
    this.switchLanguage = function (languageKey) {
      logger.debug("Switching language!");
      $translate.use(languageKey);
    };
          
  }
    
  langController.$inject = ['$translate']; 

  return langController;
});
  


    
      
    
  



