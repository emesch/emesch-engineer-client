'use strict';

define(['logger'], function(logger) {
  
  function authenticationService($window, $http) {
      
    return {
      performRequest: performRequest,
      isLoggedIn: isLoggedIn
    };
    
    function performRequest(method, url, data, doWhenSuccess, doWhenFailed) {
      var request = prepareRequest(method, url, data);
      $http(request).then(doWhenSuccess, doWhenFailed);
    }
      
    function isLoggedIn() {
      logger.debug("Checking, if user is logged in...");
        if($window.localStorage['jwtToken']){
          logger.debug("JWT is in localStorage");
          return true;
        }
    }
    
    function prepareRequest(method, url, data) {
      var req = {};      
      req.method = method;
      req.url = url;
      if(data != null) {
        req.data = data;  
      }      
      
      if(isLoggedIn()) {
        req.headers = {
          'Authorization': 'Bearer '+$window.localStorage['jwtToken']
        }
      }      
      
      return req;
      
    }
  }
  
  authenticationService.$inject = ['$window', '$http'];
  
  return authenticationService;
  
}); 
  
