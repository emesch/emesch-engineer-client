'use strict';

define(
  ['logger', 'translationModuleConfig'],
  function(logger, translationModuleConfig){
    logger.debug("Defining module TranslationModule...");
    angular.module('TranslationModule', ['pascalprecht.translate'])
      .config(translationModuleConfig);   
    
  }
);

