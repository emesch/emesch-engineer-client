define(['logger'], function(logger) {
  
  function myCoursesController(coursesService) {
    
    var self = this;
    self.myCourses = null; 
    
    self.goToCourse = function(courseData) {      
      coursesService.goToCoursePage(courseData.id);
    };    
    
    coursesService.getMyCoursesJson().then(function(data) {      
      self.myCourses = data;
      console.log(data);
    });    
  }
  
  myCoursesController.$inject = ['coursesService'];
  
  return myCoursesController;
  
});