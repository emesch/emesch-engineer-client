'use strict';

define(['logger'], function(logger) {

  function coursesService(envService, authenticationService, $state, $q) {
    
    var domainUrl = envService.read('domainUrl');
    var createCourseApiUrl = envService.read('createCourseApiUrl');
    var getCoursesApiUrl = envService.read('getCoursesApiUrl');
    var getPopularCoursesApiUrl = envService.read('getPopularCoursesApiUrl');
    var getMyCoursesApiUrl = envService.read('getMyCoursesApiUrl');    
    
    return {
      createNewCourse: createNewCourse,
      goToCoursePageByResponseData: goToCoursePageByResponseData,
      goToCoursePage: goToCoursePage,
      
      getCourseJson: getCourseJson,
      getCourseJsonById: getCourseJsonById,      
      getAllCoursesJson: getAllCoursesJson,
      getPopularCoursesJson: getPopularCoursesJson,
      getMyCoursesJson: getMyCoursesJson
    };    
    
    function createNewCourse(newCourseData, doWhenCourseCreationSuccess, doWhenCourseCreationFailed) {      
      authenticationService.performRequest('POST', createCourseApiUrl, newCourseData, doWhenCourseCreationSuccess, doWhenCourseCreationFailed);
    }
    
    function goToCoursePageByResponseData(courseId, responseData) {
      var responseView = responseData.view;
      var responseElements = responseData.elements;
      var responseErrors = responseData.errors;
      
      $state.go('coursePage', {courseId: courseId, view: responseView, elements: responseElements, errors: responseErrors});
    }
    
    function goToCoursePage(courseId) {      
      getCourseJsonById(courseId, function(response){
        goToCoursePageByResponseData(courseId, response.data)  
      }, function(response) {
        $state.go('browseCourses');   
      });      
    }
    
    function getCourseJson(apiUrl, doWhenGettingCourseSuccess, doWhenGettingCourseFailed) {      
      authenticationService.performRequest('GET', domainUrl+apiUrl, null, doWhenGettingCourseSuccess, doWhenGettingCourseFailed);  
    }
    
    function getCourseJsonById(courseId, doWhenGettingCourseSuccess, doWhenGettingCourseFailed) {
      var jsonUrl = domainUrl+getCoursesApiUrl+"/"+courseId;
      authenticationService.performRequest('GET', jsonUrl, null, doWhenGettingCourseSuccess, doWhenGettingCourseFailed);    
    }    
    
    function getAllCoursesJson() {
            
    }
    
    function getPopularCoursesJson() {
      var deferred = $q.defer();
      var jsonUrl = domainUrl+getPopularCoursesApiUrl;
      
      authenticationService.performRequest('GET', jsonUrl, null, function(response) {        
        deferred.resolve(response.data.elements.courses.value);
      }, function() {
        deferred.reject();
      });
                                           
      return deferred.promise;      
    }
    
    function getMyCoursesJson() {
      var deferred = $q.defer();
      var jsonUrl = domainUrl+getMyCoursesApiUrl;
			
      authenticationService.performRequest('GET', jsonUrl, null, function(response) {        
        deferred.resolve(response.data.elements.courses.value);
      }, function() {
        deferred.reject();
      });
                                           
      return deferred.promise;
    }
    
  }
  
  coursesService.$inject = ['envService', 'authenticationService', '$state', '$q'];
  
  return coursesService;
  
});