require
  .config({
    
    paths: {
      //libs:
      'angular': ['https://ajax.googleapis.com/ajax/libs/angularjs/1.5.7/angular.min', 
                  'lib/angular-1.5.7/angular.min'],
      'angularTranslate': 'lib/angular-translate.min',
      'angularTranslateLoaderStaticFiles': 'lib/angular-translate-loader-static-files.min',
      'jquery': ['https://code.jquery.com/jquery-1.12.4.min', 
                 'lib/jquery-1.12.4.min'],
      'bootstrap': ['https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min', 
                    'lib/bootstrap-3.3.6/dist/js/bootstrap.min'],
      uiBootstrap: ['lib/ui-bootstrap-tpls-2.5.0.min'],
      'uiRouter': ['https://cdnjs.cloudflare.com/ajax/libs/angular-ui-router/0.3.1/angular-ui-router.min', 
                   'lib/ui-router-0.3.1/release/angular-ui-router.min'],
      'angularEnvironment': 'lib/node_modules/angular-environment/src/angular-environment',
      'ngAnimate': ['https://ajax.googleapis.com/ajax/libs/angularjs/1.5.10/angular-animate'],
      'logger': ['lib/logger.min', 'lib/logger'],
      'angularFileUpload': ['lib/angular-file-upload.min'],
    
      
      
      //app module:
      'app': 'app',
      'appConfig': 'app-config',
      'navbarController': 'modules/navbar/navbarController',      
      'authenticationService': 'modules/authentication/services/authenticationService',
      'loginController': 'modules/authentication/controllers/loginController',
      
      'coursesService': 'modules/courses/services/coursesService',
      'courseCreatorController': 'modules/courses/controllers/courseCreatorController',
      'myCoursesController': 'modules/courses/controllers/myCoursesController',
      'coursesBrowserController': 'modules/courses/controllers/coursesBrowserController',
      'popularCoursesController': 'modules/courses/controllers/popularCoursesController',
      'coursePageController': 'modules/courses/controllers/coursePageController',
      
      'lessonService': 'modules/courses/services/lessonService',
      'lessonCreatorController': 'modules/courses/controllers/lessonCreatorController',
      'lessonPageController': 'modules/courses/controllers/lessonPageController',
      'lessonElCreatorController': 'modules/courses/controllers/lessonElCreatorController',
      
      'webinarService': 'modules/webinar/services/webinarService',
      'webinarHomeController': 'modules/webinar/controllers/webinarHomeController',
      'webinarCreatorController': 'modules/webinar/controllers/webinarCreatorController',
      'webinarPageController': 'modules/webinar/controllers/webinarPageController',
      'webinarsPanelController': 'modules/webinar/controllers/webinarsPanelController',
      
      'pageHelperService': 'modules/utils/services/pageHelperService',
      
      //translationModule:
      'translationModule': 'modules/translation/translationModule',
      'langController': 'modules/translation/langController',
      'translationModuleConfig': 'modules/translation/translationModuleConfig'
    },
    shim: {
      //libs:
      'angular': ['jquery', 'bootstrap'],
      'angularEnvironment': ['angular'],
      'ngAnimate': ['angular'],
      'angularTranslate': ['angular'],
      'angularTranslateLoaderStaticFiles': ['angularTranslate'],      
      'bootstrap': ['jquery'],
      'uiBootstrap': ['angular', 'bootstrap'],
      'uiRouter': ['angular'],
      'angularFileUpload': ['angular'],
      
      
      
      //app module:
      'app': ['logger',
              'angular', 
              'uiRouter',              
              'angularTranslate', 
              'angularTranslateLoaderStaticFiles', 
              'angularEnvironment',
              'ngAnimate',
              'uiBootstrap',
              'angularFileUpload',
             
              'appConfig',
              'navbarController',
              'translationModule',
              'authenticationService',
              'loginController',
              'langController',
              
              'coursesService',
              'courseCreatorController',
              'coursesBrowserController',
              'popularCoursesController',
              'coursePageController',
              
              'lessonService',
              'lessonCreatorController',
              'lessonPageController',
              'lessonElCreatorController',
              
              'webinarService',
              'webinarHomeController',
              'webinarCreatorController',
              'webinarPageController',
              'webinarsPanelController'],  
      'navbarController': ['angular', 'loginController'],
      'authenticationService': ['angular'],
      'loginController': ['angular', 'authenticationService'],
      
      'coursesService': ['angular'],
      'courseCreatorController': ['angular', 'coursesService'], 
      'myCoursesController': ['angular', 'coursesService'],
      'coursesBrowserController': ['angular', 'coursesService'],
      'popularCoursesController': ['angular', 'coursesService'],
      'coursePageController': ['angular', 'coursesService', 'lessonService'],      
      
      'lessonService': ['angular'],      
      'lessonCreatorController': ['angular', 'lessonService'],
      'lessonPageController': ['angular', 'coursesService', 'lessonService'],
      'lessonElCreatorController': ['angular', 'lessonService'],
      
      'webinarService': ['angular'],
      'webinarHomeController': ['angular', 'webinarService'],
      'webinarCreatorController': ['angular', 'webinarService'],
      'webinarPageController': ['angular', 'webinarService'],
      'webinarsPanelController': ['angular', 'webinarService'],
      
      'pageHelperService': ['angular'], 
      
      //translationModule:
      'translationModule': ['angular', 
                            'angularTranslate', 
                            'angularTranslateLoaderStaticFiles', 
                            'translationModuleConfig'],
      'langController': ['angular', 'angularTranslate', 'angularTranslateLoaderStaticFiles', 'translationModule'],
      'translationModuleConfig': ['angular', 'angularTranslate', 'angularTranslateLoaderStaticFiles']      
      
    }
    
});

require(['translationModule', 'app', 'logger'], function(translationModule, app, logger){
  logger.useDefaults();
  logger.setLevel(logger.INFO);  
  
  app.init();
  logger.debug("App loaded.");
});
