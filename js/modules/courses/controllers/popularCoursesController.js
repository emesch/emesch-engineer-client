define(['logger'], function(logger) {
  
  function popularCoursesController(coursesService) {
    
    var self = this;
    self.popularCourses = null; 
    
    self.goToCourse = function(courseData) {      
      coursesService.goToCoursePage(courseData.id);
    };    
    
    coursesService.getPopularCoursesJson().then(function(data) {      
      self.popularCourses = data;
      console.log("Popular courses initialized:")
      console.log(data);
    });    
  }
  
  popularCoursesController.$inject = ['coursesService'];
  
  return popularCoursesController;
  
});