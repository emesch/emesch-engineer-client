'use strict';

define(['logger'], function(logger) {

  function webinarService(envService, authenticationService, $state, $q) {
    
    var domainUrl = envService.read('domainUrl');
    var getWebinarsOnlineApiUrl = envService.read('getWebinarsOnlineApiUrl');
    var getWebinarsOfflineApiUrl = envService.read('getWebinarsOfflineApiUrl');
    var createWebinarApiUrl = envService.read('createWebinarApiUrl');
    var joinWebinarApiUrl = envService.read('joinWebinarApiUrl');
    
    return {
      createNewWebinar: createNewWebinar,
      startWebinar: startWebinar,
      stopWebinar: stopWebinar,      
      joinWebinar: joinWebinar,      
      
      getWebinarsOnlineJson: getWebinarsOnlineJson,
      getWebinarsOfflineJson: getWebinarsOfflineJson,
      
      goToWebinarPageByResponseData: goToWebinarPageByResponseData
    };
    
    function createNewWebinar(newWebinarData, doWhenCourseCreationSuccess, doWhenCourseCreationFailed){
      authenticationService.performRequest('POST', createWebinarApiUrl, newWebinarData, doWhenCourseCreationSuccess, doWhenCourseCreationFailed);  
    }
    
    function startWebinar(){
      logger.error('Not implemented yet!');
    }
    
    function stopWebinar(){
      logger.error('Not implemented yet!');
    }
    
    function joinWebinar(webinarId){
      var jsonUrl = domainUrl+joinWebinarApiUrl+webinarId;
      authenticationService.performRequest('GET', jsonUrl, null, doWhenJoinWebinarSuccess, doWhenJoinWebinarFailed);  
    }
    
    function doWhenJoinWebinarSuccess(response){
      logger.info("Succesfully joined to webinar!");
      var webinarId = response.data.elements.webinarObject.value.id;
      goToWebinarPageByResponseData(webinarId, response.data); 
    }
    
    function doWhenJoinWebinarFailed(response) {
      logger.error("Join webinar failed!");
    }
      
    function getWebinarsOnlineJson(){
      var deferred = $q.defer();
      var jsonUrl = domainUrl+getWebinarsOnlineApiUrl;
      authenticationService.performRequest('GET', jsonUrl, null, function(response) {
          deferred.resolve(response.data.elements.webinarsOnline.value);                                   
      }, function() {
          deferred.reject();
      });
        
      return deferred.promise;    
    }
      
    function getWebinarsOfflineJson() {
      var deferred = $q.defer();
      var jsonUrl = domainUrl+getWebinarsOfflineApiUrl;
      authenticationService.performRequest('GET', jsonUrl, null, function(response) {
          deferred.resolve(response.data.elements.webinarsOffline.value);                                   
      }, function() {
          deferred.reject();
      });
        
      return deferred.promise;    
    }    
    
    function goToWebinarPageByResponseData(webinarId, responseData) {
      var responseView = responseData.view;
      var responseElements = responseData.elements;
      var responseErrors = responseData.errors;
      
      $state.go('webinarPage', {webinarId: webinarId, view: responseView, elements: responseElements, errors: responseErrors});
    }
    
  }
  
  webinarService.$inject = ['envService', 'authenticationService', '$state', '$q'];
  
  return webinarService;
  
});