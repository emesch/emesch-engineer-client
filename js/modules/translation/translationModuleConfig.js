'use strict'

define(
  [],
  function(){
  
    function translationModuleConfig($translateProvider) {
    
      //needs StaticFilesLoader extension
      $translateProvider.useStaticFilesLoader({
        prefix: 'locale/lang-',
        suffix: '.json'
      });
    
      $translateProvider.preferredLanguage('en');    
      $translateProvider.useSanitizeValueStrategy('sanitizeParameters'); 
    
    }
  
    translationModuleConfig.$inject = ['$translateProvider'];
  
    return translationModuleConfig;

  }
);