define(['logger'], function(logger) {
  
  function lessonCreatorController($state, $stateParams, $uibModalInstance, lessonService) {
    
    var self = this;
    
    self.courseObject = null;
    
    self.alerts = [];
    
    self.newLesson = {
      courseId: undefined,
      chapterId: undefined,
      title: undefined,
      description: undefined      
    };
    
    self.onCreateLesson = function() {
      sendRequestWithNewLessonData();      
    }
    
    self.onCreateLessonAndClose = function() {
      sendRequestWithNewLessonData();      
    }
    
    self.onCancelCreation = function() {      
      $uibModalInstance.dismiss('cancel');    
    }
    
    self.getChapters = function() {      
      if(self.courseObject) {
        return self.courseObject.value.chapters;
      }
    };    
    
    self.closeAlert = function() {
      self.alerts.splice(0, 1); 
    }
    
    init();   
    
    function init() {
      console.log('lessonCreatorController initialization'); 
      self.courseObject = $stateParams.elements.courseObject; 
    }
    
    function sendRequestWithNewLessonData() {
      var newLessonData = {
        courseId: self.courseObject.value.id,
        chapterId: self.newLesson.chapterId,
        title: self.newLesson.title,
        description: self.newLesson.description        
      };      
      
      lessonService.createNewLesson(newLessonData, doWhenLessonCreationSuccess, doWhenLessonCreationFailed);      
    }
    
    function doWhenLessonCreationSuccess(response) {
      logger.debug("Lesson creation succes! ");      
      $uibModalInstance.close();
    }

    function doWhenLessonCreationFailed(response) {     
      logger.error("Lesson creation failed! ");
      if(response.data.errors[0]) {
        addAlert(response.data.errors[0].message);     
      }          
    }
    
    function addAlert(alertMsg) {
      self.alerts[0] = ({msg: alertMsg});
    }    
        
  }
  
  lessonCreatorController.$inject = ['$state', '$stateParams', '$uibModalInstance', 'lessonService'];
  
  return lessonCreatorController;
  
});