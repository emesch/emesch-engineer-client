define(['logger'], function(logger) {
  'use strict';
  
  function loginController($translate, $http, envService, $state, $window, authenticationService) {
    
    logger.debug("Defining loginController...");
    
    var self = this;		

      self.tabIndex = 0;      

      self.registerInfo = {
        firstName: undefined,
        lastName: undefined,
        email: undefined,
        password: undefined
      };
      self.loginInfo = {
        email: undefined,
        password: undefined
      };

      self.registerUser = function() {
        var data = {  	      
          firstName: self.registerInfo.firstName,
          lastName: self.registerInfo.lastName,
          email: self.registerInfo.email,
          password: self.registerInfo.password   	
        };  	      	    
        logger.debug("Registering user data: "+data.firstName+data.lastName+data.email+data.password);
        $state.go('registerUser');
        //TODO: Prepare some endpoint
        $http.post(envService.read('registerUser'), data).then(doWhenRegistrationSuccess, doWhenRegistrationFailed); //$http.get(envService.read('usersApi')).then(doWhenRegistrationSuccess, doWhenRegistrationFailed);	
      };
      
      self.loginUser = function() {
        var data = {         
          email: self.loginInfo.email,
          password: self.loginInfo.password   	
        };
        logger.info("Authenticating user data: "+data.email+data.password);        
        $http.post("http://localhost:8182/auth", data).then(doWhenLoginSuccess, doWhenLoginFailed);
      }

      self.trigger = function() {  	  	
        self.tabIndex == 0? self.tabIndex = 1 : self.tabIndex = 0;  	  		  	 
      };
      
      self.getToken = function() {
        return $window.localStorage['jwtToken'];
      };
                
      self.isLoggedIn = function() {        
        return authenticationService.isLoggedIn();
      };          
  	  
  	  //PRIVATE FUNCTIONS
  	  
      function doWhenRegistrationSuccess(response) {
        logger.info("Registration success!"+response);	
      }
  	  
      function doWhenRegistrationFailed(error) {
        logger.info("Registration failed!"+error);
      }
  	  
  	  function doWhenLoginSuccess(response) {
        logger.info("Login success!");
        logger.debug(self.getToken());
        saveToken(response.data.token);
        logger.debug(self.getToken());
        
      }

      function doWhenLoginFailed(error) {
        logger.info("Login failed!"+error);
      }
      
      function saveToken(token) {
        $window.localStorage['jwtToken'] = token;
      }
    
  }
  
  loginController.$inject = ['$translate', '$http', 'envService', '$state', '$window', 'authenticationService'];
  
  return loginController;  
  
});