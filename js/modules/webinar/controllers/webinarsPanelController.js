define(['logger'], function(logger) {
  
  function webinarsPanelController(webinarService) {
    
    var self = this;
    self.webinarsOnline = null;
    self.webinarsOffline = null;
    
    self.joinWebinar = function(webinar) {
      logger.info("Joining webinar");
      logger.info(webinar);
      webinarService.joinWebinar(webinar.id);
    };    
    
    webinarService.getWebinarsOnlineJson().then(function(data) {      
      self.webinarsOnline = data;
      console.log("Webinars online initialized.")
      console.log(data);
    });
      
    webinarService.getWebinarsOfflineJson().then(function(data) {
      self.webinarsOffline = data;
      console.log("Webinars offline initialized.")
      console.log(data);    
    }); 
  }
  
  webinarsPanelController.$inject = ['webinarService'];
  
  return webinarsPanelController;
  
});