'use strict';

define(
       ['appConfig',
        'navbarController',
        'loginController',
        'authenticationService',
        'langController',
        'logger',
        
        'coursesService',
        'courseCreatorController',
		'myCoursesController',
        'coursesBrowserController',
        'popularCoursesController',
        'coursePageController',
        
        'lessonService',
        'lessonCreatorController',
        'lessonPageController',
        'lessonElCreatorController',
        
        'webinarService',
        'webinarHomeController',
        'webinarCreatorController',
        'webinarPageController',
        'webinarsPanelController',
       
        'pageHelperService'], 
        function(
                  appConfig, 
                  navbarController, 
                  loginController, 
                  authenticationService, 
                  langController, 
                  logger, 
                  
                  coursesService, 
                  courseCreatorController, 
                  myCoursesController, 
                  coursesBrowserController, 
                  popularCoursesController, 
                  coursePageController,
                  
                  lessonService,
                  lessonCreatorController,
                  lessonPageController,
                  lessonElCreatorController, 
                  
                  webinarService,
                  webinarHomeController,
                  webinarCreatorController,
                  webinarPageController,
                  webinarsPanelController,
                  
                  pageHelperService) {
        
          logger.debug("Defining module app...");          
          var app = angular.module('app', ['ui.router', 'ui.bootstrap', 'TranslationModule', 'environment', 'ngAnimate', 'angularFileUpload']);  
          
          app.config(appConfig);
          app.controller('langController', langController); 
          app.controller('navbarController', navbarController);
          
          app.controller('loginController', loginController);
          app.factory('authenticationService', authenticationService);
          
          app.factory('coursesService', coursesService);
          app.controller('courseCreatorController', courseCreatorController);
		  app.controller('myCoursesController', myCoursesController);
          app.controller('coursesBrowserController', coursesBrowserController);
          app.controller('popularCoursesController', popularCoursesController)
          app.controller('coursePageController', coursePageController);
          
          app.factory('lessonService', lessonService);          
          app.controller('lessonCreatorController', lessonCreatorController);
          app.controller('lessonPageController', lessonPageController);
          app.controller('lessonElCreatorController', lessonElCreatorController);
          
          app.factory('webinarService', webinarService);
          app.controller('webinarHomeController', webinarHomeController);
          app.controller('webinarCreatorController', webinarCreatorController);
          app.controller('webinarPageController', webinarPageController);
          app.controller('webinarsPanelController', webinarsPanelController);
                    
          app.factory('pageHelperService', pageHelperService);          
          
          app.init = function() {
            logger.debug("Bootstraping app...");            
            angular.bootstrap(document, ['app']);  
            logger.debug("After initialization.");
          }
                    
          return app;        
  
});   
